'use strict';
var fs = require('fs');
var path = require('path');
var grunt = require('grunt');

var FONT_WEIGHT = {
  light: 200,
  normal: 400,
  medium: 500,
  semiBold: 600,
  bold: 700,
  heavy: 800
};

var FontUtils = {
  parsedFonts: {},
  fontFileMap: {},
  parseFontManifest: function() {
    var parseString = require('xml2js').parseString;
    var xml = fs.readFileSync('src/fontlist.xml');
    parseString(xml, function (err, result) {
        if (!result.fonts.font || !Array.isArray(result.fonts.font)) {
          console.log('Invalid XML');
          throw 'Invalid XML';
        }
        FontUtils.parsedFonts = {};
        result.fonts.font.forEach(function(font){
          font.$.FontStyle = FontUtils.getCssStyle(font.$);
          font.$.FontWeight = FontUtils.getCssWeight(font.$);
          FontUtils.parsedFonts[font.$.psName] = font.$;
          FontUtils.fontFileMap[font.$.woff] = font.$.psName;
          FontUtils.fontFileMap[font.$.eot] = font.$.psName;
          FontUtils.fontFileMap[font.$.ttf] = font.$.psName;
          FontUtils.fontFileMap[font.$.svg] = font.$.psName;
        });
        // console.log(FontUtils.parsedFonts);
        return FontUtils.parsedFonts;
    });
  },
  getCssStyle: function(font) {
    var style = 'normal';
    if (font.displayName.indexOf('Italic') >= 0) {
      style = 'italic';
    }
    return style;
  },
  getCssWeight: function(font) {
    var weight = FONT_WEIGHT.normal;
    if (font.displayName.indexOf('SemiBold') >= 0) {
      weight = FONT_WEIGHT.semiBold;
    }
    else if (font.displayName.indexOf('Bold') >= 0) {
      weight = FONT_WEIGHT.bold;
    }
    else if (font.displayName.indexOf('Light') >= 0) {
      weight = FONT_WEIGHT.light;
    }
    else if (font.displayName.indexOf('Medium') >= 0) {
      weight = FONT_WEIGHT.medium;
    }
    else if (font.displayName.indexOf('Heavy') >= 0) {
      weight = FONT_WEIGHT.heavy;
    }
    return weight;
  },
  getFontById: function (id) {
    return FontUtils.parsedFonts[id];
  },
  getFontByFile: function(fileName) {
    return FontUtils.parsedFonts[FontUtils.fontFileMap[fileName]]
  },
  getNewFileName: function(dest, oldFileName) {
    var fileName = oldFileName;
    if (oldFileName.indexOf('.') > 0) {
      var fontFileName = path.basename(oldFileName);
      var font = FontUtils.getFontByFile(fontFileName);
      var ext = path.extname(oldFileName);
      fileName = oldFileName.replace(fontFileName, font.psName + ext);
    }
    return path.join(dest, fileName);
  },
  renderTemplate: function(template, data) {
    var source = fs.readFileSync(path.join('template', template));
    var template = require('handlebars').compile(source.toString());
    // console.log(data);
    return template(data);
  },
  writeCss: function(cssFile, htmlFile, baseUrl) {
    var keys = Object.keys(FontUtils.parsedFonts);
    var fonts = keys.map(function(val) { return FontUtils.parsedFonts[val];});
    fonts.sort(function compare(a,b) {
      return a.displayName.localeCompare(b.displayName);
    });
    var css = FontUtils.renderTemplate('lol-fonts.css.hb', {
      fonts: fonts
    });
    fs.writeFileSync(cssFile, css);
    var html = FontUtils.renderTemplate('demo.html.hb', {
      fonts: fonts,
      cssUrl: baseUrl + '/lol-fonts.css'
    });
    fs.writeFileSync(htmlFile, html);
  }
}

module.exports = FontUtils;
