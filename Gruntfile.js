var myfonts = require('./helpers/fonts');

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    baseUrl: '',
    pkg: grunt.file.readJSON('package.json'),
    clean: ['src', 'dist'],
    unzip: {
      src: 'lol.com_fonts_package-*.zip',
    },
    copy: {
      main: {
        expand: true,
        cwd: 'src',
        src: 'Fonts/**',
        dest: 'dist',
        rename: myfonts.getNewFileName
      }
    },
    css: {
      dev: '',
      prod: '//s.lolstatic.com/awesomefonts/<%= pkg.version %>'
    },
    watch: {
      options: {
        livereload: true,
      },
      zip: {
        files: ['lol.com_fonts_package-*.zip'],
        tasks: ['unzip:src']
      },
      src: {
        files: ['src/Fonts/**'],
        tasks: ['copy']
      },
      css: {
        files: ['src/*.xml', 'dist/Fonts/*', 'template/**'],
        tasks: ['manifest', 'css:dev']
      },
      devel: {
        files: ['helpers/**'],
        tasks: ['manifest', 'css:dev'],
        options: {
          atBegin: true
        }
      }
    },
    connect: {
      devel: {
        options: {
          port: '8338',
          base: 'dist',
          open: 'http://localhost:8338/demo.html',
          // open: true,
          livereload: 35729
        }
      }
    },
    bump: {
      options: {
        updateConfigs: ['pkg'],
        pushTo: 'origin'
      }
    },
    aws: {
      config_file: grunt.option('aws-config') || '$HOME/.aws/web.s3cfg'
    },
    exec: {
      deploy: [
        'echo releasing version <%= pkg.version %> to s3...',
        's3cmd sync -r --no-preserve --acl-public --force' +
        ' --config=<%= aws.config_file %> --guess-mime-type' +
        ' dist/' +
        ' s3://riot-web-cdn/awesomefonts/<%= pkg.version %>/'
      ].join('&&')
    }
  });

  // grunt.loadTasks('tasks');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-zip');
  grunt.loadNpmTasks('grunt-bump');
  grunt.loadNpmTasks('grunt-exec');


  grunt.registerTask('devel', ['connect', 'watch']);
  grunt.registerTask('manifest', function() {
    myfonts.parseFontManifest();
  });

  grunt.registerMultiTask('css', 'Generate CSS', function() {
    myfonts.writeCss('dist/lol-fonts.css', 'dist/demo.html', this.data);
  });

  grunt.registerTask('version', 'Versioning tasks', function(){
    var target = '';
    if (this.args[0] !== undefined) {
      target = this.args[0];
    }
    switch(target){
      case '':
      case 'current':
        console.log(grunt.config('pkg').version);
        break;
      case 'bump':
        grunt.task.run(this.args.join(':'));
        break;
      default:
        grunt.fail.fatal('target not found: ' + target);
    }
  });

  grunt.registerTask('build', [
    'clean',
    'unzip:src',
    'manifest',
    'copy',
    'css:prod'
  ]);

  grunt.registerTask('default', 'devel');

  grunt.registerTask('deploy', ['exec:deploy']);
  grunt.registerTask('release', ['version:bump', 'deploy']);
};
