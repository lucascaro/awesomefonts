<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Webfonts Demo</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
      body, textarea { font-family:'Spiegel-Regular', Arial, Sans-Serif; line-height:1.6em; font-size:13px; }
      h1 { font-size:3em; font-family:"BeaufortforLOL-Bold"; }
      h2, h3 { color:#999; }
      .content { width:780px; margin: 20px auto 40px; }
      .demo { font-weight:bold; font-style:normal; color: #999 }
      .fontdisplay {font-size:2em;line-height:150%;margin: 10px 40px 0px 0px;}
      .guidelines { color:#333; }
      pre { padding:0 0 0 20px; }
      hr { border:0; border-bottom:1px solid #537094; margin:20px 0; }
      .link { color: #537094;text-decoration:none; font-weight:bold;}
      .uc {text-transform:uppercase}
      pre.snippet {
        float:right;
        font-size:13px;
        line-height:1.1em;
        width:260px;
      }
      pre.html-snippet {
        padding:5px 20px;
      }
      hr {
        clear:both;
      }
    </style>
    <link type="text/css" rel="stylesheet" href="{{cssUrl}}"></link>
</head>
<body>
    <div class="content">
        <h1 class="demo">LoL.com Web fonts</h1>
        <p style="margin-top:-10px; font-size:1.5em;color:#666"><b>@font-face implementation instructions</b></p>
        <hr />
        <p class="uc">To use these fonts, add this to your html:</p>
        <pre class="html-snippet">&lt;link type="text/css" rel="stylesheet" href="{{cssUrl}}" &gt;&lt;/link&gt;</pre>

        <p class="uc">Or in your stylesheet:</p>
        <pre class="html-snippet">@import:url("{{cssUrl}}");</pre>
        <p class="uc">The fonts contained in this kit are:</p>
        <div class="fontdisplay">
        {{#fonts}}
          <div class="font" style="clear:both;height:auto;">
<pre class="snippet">
font-family:"{{psName}}";
</pre>
            <div style="
                font-family:'{{{psName}}}';">{{psName}}</div>
          </div>
          <div class="font" style="clear:both;height:auto;">
<pre class="snippet">
font-family:"{{FamilyName}}";
font-weight: {{FontWeight}};
font-style: {{FontStyle}};
</pre>
            <div style="
                font-family:'{{FamilyName}}';
                font-weight: {{FontWeight}};
                font-style: {{FontStyle}};
                ">{{displayName}}</div>
          </div>
          <hr />
        {{/fonts}}

        </div>
        <hr />
    </div>
</body>
</html>
