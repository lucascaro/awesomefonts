@import url("http://fast.fonts.net/t/1.css?apiType=css&amp;projectid=78ccb4a5-3b82-490f-9b9c-25706122b3bb");
{{#fonts}}
/* {{displayName}} */
@font-face{
  font-family:"{{FamilyName}}";
  src:url("Fonts/{{psName}}.eot?#iefix");
  src:url("Fonts/{{psName}}.eot?#iefix") format("eot"),url("Fonts/{{psName}}.woff") format("woff"),url("Fonts/{{psName}}.ttf") format("truetype"),url("Fonts/{{psName}}.svg#{{id}}") format("svg");
  font-weight:{{FontWeight}};
  font-style:{{FontStyle}};
}
@font-face{
  font-family:"{{psName}}";
  src:url("Fonts/{{psName}}.eot?#iefix");
  src:url("Fonts/{{psName}}.eot?#iefix") format("eot"),url("Fonts/{{psName}}.woff") format("woff"),url("Fonts/{{psName}}.ttf") format("truetype"),url("Fonts/{{psName}}.svg#{{id}}") format("svg");
}
{{/fonts}}
